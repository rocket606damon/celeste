<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class='footerlogo'>
          <img src="<?php bloginfo('url'); ?>/media/celeste-logo.png" alt="Celeste Logo">
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <h4>Questions? Comments? Get in touch:</h4>
<!--         <p>PINNACLE FOODS GROUP
          <br>PO BOX 971 - Miami, FL 33152
        </p> -->
        <p>For inquiries regarding Celeste or its products,
          <br>please <a href="http://pinnaclefoods.com/company/contact-us" target="_blank">click here</a> or call us at <a href="tel:+1-800-768-6287">1-800-768-6287</a>
        </p>
      </div>
    </div><!--Row-->
  </div><!--Container-->
  <div class="container-fluid">
    <div class="row d-flex justify-content-center">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 legal">
        <span class="d-sm-block d-md-inline">© 2018 Pinnacle Foods Group LLC. All rights reserved.</span>
        <span><a href="http://pinnaclefoods.com/company/terms-of-use" target="_blank">Terms &amp; conditions</a></span>
        <span class="d-none d-sm-inline">|</span>
        <span><a href="http://pinnaclefoods.com/company/privacy" target="_blank">Privacy Statement</a></span>
        <span class="d-none d-sm-inline">|</span>
        <span><a href="/product-locator/">Product Locator</a></span>
        <span class="d-none d-sm-inline">|</span>
        <span><a href="http://pinnaclefoods.com/working-here#careers" target="_blank">Careers</a></span>
      </div>
    </div>
    <!--Row-->
  </div>
  <!--Container-->
 </footer>
