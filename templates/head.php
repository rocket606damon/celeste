<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Celeste® | Pizza For One</title>
	<?php if(is_front_page()): ?>
	<meta name="ps-key" content="1793-586ec841ad7d2dbe23d3d2b6">
	<meta name="ps-country" content="US">
	<meta name="ps-language" content="en">
	<script src="//cdn.pricespider.com/1/lib/ps-widget.js" async></script>
	<?php endif; ?>  
	<?php if(is_page('product-locator')): ?>
	<meta name="ps-key" content="1793-58adad35a86c126bc0e6bf6a">
	<meta name="ps-country" content="US">
	<meta name="ps-language" content="en">
	<script src="//cdn.pricespider.com/1/lib/ps-widget.js" async></script>
	<?php endif; ?>
  <?php wp_head(); ?>
</head>
