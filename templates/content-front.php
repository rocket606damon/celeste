<!--==================================================
 Rotating Carousel
==================================================-->
<section id="hero" class="clearfix">
  <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="6000">
    <div class="carousel-item active">
      <div class="overlay hidden-md-up"></div>
      <picture>
        <source srcset="<?php bloginfo('url'); ?>/media/celeste-hero-min.png" media="(min-width: 1400px)">
        <source srcset="<?php bloginfo('url'); ?>/media/celeste-hero-1400-min.png" media="(min-width: 769px)">
        <source srcset="<?php bloginfo('url'); ?>/media/celeste-hero-760.png" media="(min-width: 577px)">
        <img srcset="<?php bloginfo('url'); ?>/media/celeste-hero-760.png" alt="responsive image" class="d-block img-fluid">
      </picture>
      <div class="carousel-caption">
        <div class="cta">
          <h1><strong>Authentic Italian Since 1930</strong></h1>
          <a class="js-scroll-trigger" href="#our-story"><span class="btn btn-primary btn-lg">Our Story</span></a>
        </div>
      </div>
    </div>
  </div>
</section>


<!--==================================================
 Microwave in Minutes Banner Callout
==================================================-->

<section id="banner">
  <div class="container-fluid bannerTop">
    <div class="row">
      <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 item2">
        <div class="microwave-callout">
          <img src="<?php bloginfo('url'); ?>/media/microwave2.png" alt="Microwave">
          <img class="blink" src="<?php bloginfo('url'); ?>/media/microwave1.png" alt="Microwave">
        </div>
      </div>
      <div class="col-lg-7 offset-lg-5 col-md-7 col-sm-12 col-xs-12 item1">
        <div class="words">
          <!-- <p>New Heating Instructions</p> -->
          <h2>MICROWAVES IN 2 MINUTES!</h2>

        </div>
      </div>
    </div>
  </div>
  <div class="container-fluid goldbar"></div>
</section>
<!--==================================================
 Pizza Boxes
==================================================-->

<section id="products">
  <div class="tablecloth"></div>
  <div class="container">
    <!-- Large pizza + description -->
    <div id="lg-slider" class="flexslider">
      <ul class="slides">
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-cheese.png" alt="Original Cheese Pizza">
            <h2 class="text-center">Authentic Italian since 1930</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600046301"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-zestycheese.png" alt="Zesty 4 Cheese Pizza">
            <h2 class="text-center">A zesty blend of delicious cheeses</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600044000"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-fourcheese.png" alt="Original 4 Cheese Pizza">
            <h2 class="text-center">Four different cheeses blend together for heaven in a bite</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600050308"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-pepperoni.png" alt="Pepperoni Pizza">
            <h2 class="text-center">Made with pork - chicken added</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600046103"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-suprema.png" alt="Suprema Pizza">
            <h2 class="text-center">With sausage, mushroom, pepperoni, green and red peppers &amp; onions</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600046707"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-sausagepepperoni.png" alt="Sausage &amp; Pepperoni Pizza">
            <h2 class="text-center">Sausage &amp; pepperoni made with pork - chicken added</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600046202"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-sausage.png" alt="Sausage Pizza">
            <h2 class="text-center">Made with pork - chicken added</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600045700"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-vegetable.png" alt="Vegetable Pizza">
            <h2 class="text-center">With green &amp; red peppers, mushrooms, onions &amp; olives</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600045007"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-deluxe.png" alt="Deluxe Pizza">
            <h2 class="text-center">With sausage, green and red peppers &amp; mushrooms. Sausage made with pork, chicken added</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600045809"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-Mushroom.png" alt="Mushroom Pizza">
            <h2 class="text-center">with mushrooms</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600047605"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-white.png" alt="White Pizza">
            <h2 class="text-center">White sauce seasoned with garlic &amp; spices</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600046806"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-cheesygarlic.png" alt="Cheesy Garlic Breadsticks">
            <h2 class="text-center">Cheesy Garlic Breadsticks</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600041009"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-5PACK-cheese.png" alt="5-Pack Cheese Pizza">
            <h2 class="text-center">5-Pack Cheese Pizza</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600046950"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i>  Where To Buy</span> -->
            </div>
          </div>
        </li>
        <li>
          <div class="container-fluid">
            <img src="<?php bloginfo('url'); ?>/media/lg-5PACK-pepperoni.png" alt="5-Pack Pepperoni Pizza">
            <h2 class="text-center">5-Pack Pepperoni Pizza</h2>
            <div class="text-center where-to-buy">
              <div class="ps-widget btn-light-blue" ps-sku="19600047001"></div>
              <!-- <span class="btn btn-lg btn-primary"><i class="fa fa-map-marker" aria-hidden="true"></i> Where To Buy</span> -->
            </div>
          </div>
        </li>
      </ul>
    </div>

    <div id="carousel" class="flexslider">
      <!-- smaller pizza + name -->
      <ul class="slides">
        <li>
          <img src="<?php bloginfo('url'); ?>/media/cheese.png" alt="Original Cheese Pizza">
          <p class="text-center">Original Cheese Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/zestycheese.png" alt="Zesty 4 Cheese Pizza">
          <p class="text-center">Zesty 4 Cheese Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/fourcheese.png" alt="Original 4 Cheese Pizza">
          <p class="text-center">Original 4 Cheese Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/pepperoni.png" alt="Pepperoni Pizza">
          <p class="text-center">Pepperoni Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/suprema.png" alt="Suprema Pizza">
          <p class="text-center">Suprema Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/sausagepepperoni.png" alt="Sausage &amp; Pepperoni Pizza">
          <p class="text-center">Sausage &amp; Pepperoni Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/sausage.png" alt="Sausage Pizza">
          <p class="text-center">Sausage Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/vegetable.png" alt="Vegetable Pizza">
          <p class="text-center">Vegetable Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/Deluxe.png" alt="Deluxe Pizza">
          <p class="text-center">Deluxe Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/Mushroom.png" alt="Mushroom Pizza">
          <p class="text-center">Mushroom Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/white.png" alt="White Pizza">
          <p class="text-center">White Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/cheesygarlic.png" alt="Cheesy Garlic Breadsticks">
          <p class="text-center">Cheesy Garlic Breadsticks</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/5PACK-cheese.png" alt="5-Pack Cheese Pizza">
          <p class="text-center">5-Pack Cheese Pizza</p>
        </li>
        <li>
          <img src="<?php bloginfo('url'); ?>/media/5PACK-pepperoni.png" alt="5-Pack Pepperoni Pizza">
          <p class="text-center">5-Pack Pepperoni Pizza</p>
        </li>
        <li>
      </ul>
    </div>
  </div><!-- /.container -->
</section><!-- /#products -->

<!--==================================================
 OUR STORY SECTION STARTS
==================================================-->

  <section id="our-story">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-5 col-md-3 col-sm-3 col-xs-3 d-none d-lg-block">
          <img src="<?php bloginfo('url'); ?>/media/our-story-left.png" alt="Celeste Pizza Boxes">
        </div>
        <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12">
          <h2 class="text-center">Our Story</h2>
          <img class="text-center mama" src="<?php bloginfo('url'); ?>/media/mama-celeste.png" alt="Mama Celeste">
        </div>
        <div class="col-lg-5 col-md-3 col-sm-3 col-xs-3 d-none d-lg-block">
          <img src="<?php bloginfo('url'); ?>/media/our-story-right.png" alt="Celeste Pizza Boxes">
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12 centered">
          <div class="story">
            <p>The first heavenly Celeste<sup>®</sup> pizzas were made in the 1930s when Celeste Lizio, an immigrant from Southern Italy, opened her first pizzeria in Chicago.
            </p>
            <p>Her recipes were a local sensation. As demand grew, she began to supply pizza to restaurants throughout the region. By the 1940s, business was so good that she closed the restaurant and worked solely on supplying pizza and pizza-making ingredients
              to pizzerias throughout the Midwest. Eventually, she sold the recipes to the Quaker Oats Company</p>
            <p>Over the past 40 years, Celeste has become a staple in Chicago, Florida, California, and the northeastern U.S. Celeste is now one of the top selling brands in the nation.</p>
            <p>The picture of “Mama Celeste” on every box attests to our commitment to her original promise of homemade-tasting, authentic, Italian pizza. The brand continues to deliver restaurant-quality flavor, and provides value in the traditional sense:
              a quality product at an affordable price.</p>
            <img class="authentic" src="<?php bloginfo('url'); ?>/media/authentic.svg" alt="Authentic Italian Since 1930">
          </div>
        </div>
      </div><!--.row-->
    </div><!--.container-->
  </section><!--#our-story-->

<!--==================================================
 PIZZA FOR ONE
==================================================-->
<section id="social-banner">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 item1">
        <h2><i>pizza for one<sub>&#8482;</sub></i></h2>
        <!-- <p>follow us on:</p>
        <div class="socials">
          <a href="#"><img src="<?php //bloginfo('url'); ?>/media/fb.svg" alt="facebook icon"></a>
          <a href="#"><img src="<?php //bloginfo('url'); ?>/media/twitter.svg" alt="twitter icon"></a>
          <a href="#"><img src="<?php //bloginfo('url'); ?>/media/instagram.svg" alt="instagram icon"></a>
        </div> -->
      </div>
    </div>
  </div>
</section>
