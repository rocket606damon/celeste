<header id="navbar">
  <nav class="navbar navbar-toggleable-sm navbar-inverse navbar-fixed-top col-lg-12 bg-inverse">
    <div class="container clearfix">
    <button class="navbar-toggle navbar-toggle-right collpased" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo home_url(); ?>"><img src="<?php bloginfo('url'); ?>/media/celeste-logo2.png" alt="Celeste Logo"></a>
    <div class="clearfix nudge"></div>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item active">
          <a class="nav-link js-scroll-trigger" href="<?php if(!is_front_page()) echo get_bloginfo('url') . '/'; ?>#navbar">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="<?php if(!is_front_page()) echo get_bloginfo('url') . '/'; ?>#products">Products</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="<?php if(!is_front_page()) echo get_bloginfo('url') . '/'; ?>#our-story">Our Story</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/product-locator/"><i class="fa fa-map-marker" aria-hidden="true"></i> Store Locator</a>
        </li>
      </ul>
    </div>
  </div>
  </nav>
</header>
