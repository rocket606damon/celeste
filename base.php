<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <!--<style>
  /*Pat edits to line 163*/
  body{
    overflow-x: hidden;
    font-family: 'Museo Sans', Arial;
  }
  #navbar nav .navbar-collapse li:hover a{
    color: #fff!important;
    /* padding-bottom: 4px!important; */
    border-bottom: 2px solid #c32f24;
  }
  #hero{
    margin-top:90px;
  }
  #hero h1 {
    font-size: 4em;
    width:100%;
  }
  #hero a{
    float:right;
  }
  #hero img{
    max-width:1920px;
  }
  #hero .carousel-item{
    background-color:black;
  }
  #hero .carousel-caption {
    align-items: flex-start;
  }
  #hero .cta{
    width:65%;
    margin-top:5%;
  }
  #hero .btn{
    padding: .75rem 1.25rem;
    font-weight:700;
  }

  #products .ps-widget[ps-sku], .ps-widget[data-ps-sku]{
    font-family: 'Museo Sans'
  }
  #products .ps-widget.btn-light-blue{
    padding: .25rem 0;
    background-color:#31b0d5!important;
    transition: all .2s;
  }
  #products .ps-widget.btn-light-blue:hover{
    background-color:#01549b!important;
  }
  #products p{
    font-size:14px;
  }
  #products #carousel.flexslider img{
    height:180px;
  }
  #products .flex-direction-nav a{
    opacity:1;
    color: #fff;
    background-color: #c32f24;
    border-radius: 50%;
    text-align: center;
    border:2px solid black;
  }
  #products .flex-direction-nav .flex-next{
    right:0;
  }
  #products .flex-direction-nav .flex-prev{
    left:0;
  }
  #products #lg-slider.flexslider .slides>li{
    -webkit-backface-visibility:visible;
  }

  #our-story .row{
    display: flex;
    flex-wrap: wrap;
  }
  #our-story .container-fluid{
    position: relative;
  }


  #social-banner{
    height:400px;
  }
  #social-banner .row{
    display:flex;
  }
  .flex-direction-nav a:before {
    font-family: "flexslider-icon";
    font-size: 20px;
    display: inline-block;
    content: '\f001';
    color: #FFF;
    text-shadow: 1px 1px 0 rgba(255, 255, 255, 0.3);
    line-height: 40px;
  }
  .flex-direction-nav .flex-next {
    right: -50px;
    text-align: center;
  }
  #products #lg-slider .flex-prev{
    margin-left:16%;
  }
  #products #lg-slider .flex-next{
    margin-right:16%;
    padding-left:3px;
  }
  #products #carousel .flex-direction-nav .flex-prev{
    margin-left:-4%;
  }
  #products #carousel .flex-direction-nav .flex-next{
    margin-right:-4%;
  }

  @media (max-width: 1200px) {
    #hero h1{
      font-size: 4em;
    }
    #products #carousel .flex-direction-nav .flex-prev{
      margin-left:-2%;
    }
    #products #carousel .flex-direction-nav .flex-next{
      margin-right:-2%;
    }
  }
  @media (max-width: 992px) {
    #hero h1{
      font-size: 2.5em;
    }
    #products #lg-slider .flex-prev{
      margin-left:4%;
    }
    #products #lg-slider .flex-next{
      margin-right:4%;
    }
  }
  @media (max-width: 767px){
    #hero .cta{
      width:50%;
    }
  }
  @media (max-width: 575px) {
    #hero h1{
      font-size: 1.5em;
    }
    #hero .carousel-item a {
      margin-top:16px;
    }
    #hero .btn{
      padding: .5rem 1rem;
    }
    #hero .carousel-caption{
      left:10%;
    }
  }
/*End of Pat edits*/

  /* Product Locator Page */
  .product-locator .ps-widget{
    margin: 100px auto 60px;
  }
  .product-locator.ps-open #ps-lightbox-background,
  .home.ps-open #ps-lightbox-background{
    z-index: 1030!important;
  }
  .product-locator.ps-open .ps-lightbox,
  .home.ps-open .ps-lightbox{
    z-index: 9999!important;
  }

  /* Home Page Widget */
  .ps-widget.btn-light-blue {
    float: none!important;
    width: 230px;
    padding-left: 30px!important;
    margin: 0 auto!important;
    background-color: #0275d8!important;
    border-color: #0267bf!important;
    color: #fff!important;
    font-size: 18px!important;
    border-radius: 6px!important;
  }
  </style>-->
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
    <script src="https://use.typekit.net/eys3lep.js"></script>
    <script>
      try {
        Typekit.load({
          async: true
        });
      } catch (e) {}
    </script>
    <!-- Smooth scrolls to sections on homepage -->
    <script src="js/scrolling-nav.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
    <!-- Carousel Inits -->
    <script>
      jQuery(window).load(function() {
        //Allows users to view pizzas in a carousel

        //the main packshot
        jQuery('#lg-slider').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: true,
          slideshow: false,
          sync: "#carousel"
        });
        // The smaller packs below main packshot
        jQuery('#carousel').flexslider({
          animation: "slide",
          controlNav: false,
          animationLoop: true,
          slideshow: false,
          itemWidth: 210,
          itemMargin: 5,
          asNavFor: '#lg-slider'
        });
      });
    </script>
  </body>
</html>
